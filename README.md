# Spring Boot with Docker
This project deals with starting a spring-boot app with DB connection which is running in docker container. The DB software is not installed on the host machine
rather is pulled and run in docker to which the app connects.

#### Prerequisite
- docker must be installed along with docker-compose on the host machine

#### Issues while running docker-compose.yml
`cannot connect to the docker daemon`

Install docker-compose after installing docker (below for linux OS)
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.14.2/docker-compose-$(uname -s)-$(uname -m)"  -o /usr/local/bin/docker-compose
sudo mv /usr/local/bin/docker-compose /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```