DROP TABLE IF EXISTS users;

CREATE TABLE users (
  user_id  SERIAL PRIMARY KEY ,
  user_name varchar(45) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL
);

INSERT INTO users VALUES
(1,'Himalaya','himalaya.saxena@gmail.com','12345'),
(2,'Test User','test@gmail.com','12345'),
(3,'qwerty','qwerty@gmail.com','12345');