package example.springdocker.controller;

import example.springdocker.model.Users;
import example.springdocker.repository.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DemoController {
    @Autowired
    UsersRepo usersRepo;

    @GetMapping("/test")
    public String test(){
        return "Test successfull !!!";
    }

    @GetMapping("/users")
    public List<Users> getAllUsers(){
        return usersRepo.findAll();
    }
}
